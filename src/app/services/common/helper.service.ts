import { Injectable, ViewChild } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import Utils from '../common/utils';

import { Content } from 'ionic-angular';
@Injectable()
export class Helper {

    private helperToast: any;
    private helperAlert: any;
    private helperLoading: any;
    private helperPage: any;
    public pageLoaded: boolean = false;
    public loadingIndicatorShown: boolean = false;

    @ViewChild(Content) content: Content;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public platform: Platform,
    ) {
    };

    setPage(page) {
        this.helperPage = page;
    };


    isCordova() {
        return (this.platform.is('cordova'));
    };

    isAndroid() {
        return (this.platform.is('android'));
    };

    isIOS() {
        return (this.platform.is('ios'));
    };

    navigate(targetPageName: string, params: any, clearHistory: boolean, transitionRightToLeft: boolean) {
        
        let options: any;
        if (transitionRightToLeft == true) {
            options = { animation: 'ios-transition', animate: true, direction: 'forward' };
        } else {
            options = { animate: true, direction: 'forward' };
        }
        if (clearHistory == true) {
            this.navCtrl.push(targetPageName, params, options).then(() => {
                const index = this.navCtrl.getActive().index;
                this.navCtrl.remove(0, index);
            });
        } else {
            this.navCtrl.push(targetPageName, params, options);
        }
        
    };

    goToPage(targetPageName: string, params: any, transitionRightToLeft: boolean) {
        this.navigate(targetPageName, params, true, transitionRightToLeft);
    };

    goToSubPage(targetPageName: string, params: any, transitionRightToLeft: boolean) {
        this.navigate(targetPageName, params, true, transitionRightToLeft);
    };


}
