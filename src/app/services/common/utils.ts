import { NavController } from 'ionic-angular';

export default class Utils {

    static goToPage(navCtrl: NavController, targetPageName: string, params: any, clearHistory: boolean, moveForward: boolean) {
        let options: any;
        if (moveForward == true) {
            options = { animation: 'ios-transition', animate: true, direction: 'forward' };
        } else {
            options = { animate: true, direction: 'forward' };
        }
        if (clearHistory == true) {
            navCtrl.push(targetPageName, params, options).then(() => {
                const index = navCtrl.getActive().index;
                navCtrl.remove(0, index);
            });
        } else {
            navCtrl.push(targetPageName, params, options);
        }
    };

    static isUndefined(input) {
        if (typeof input == "undefined") {
            return true;
        } else {
            return false;
        }
    };

    static isEmpty(input) {
        if (typeof input == "undefined") {
            return true;
        } else {
            let lstrTempstring = new String(input);
            lstrTempstring = lstrTempstring.trim();
            if (lstrTempstring == "" || lstrTempstring == "undefined") {
                return true;
            } else {
                return false;
            }
        }
    };

    static isNull(input) {
        if (input != null) {
            return false;
        } else {
            return true;
        }
    };

    static isValidInput(input) {
        if (Utils.isNull(input) || Utils.isUndefined(input) || Utils.isEmpty(input)) {
            return false;
        } else {
            return true;
        }
    };

}
