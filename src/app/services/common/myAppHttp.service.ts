export default class MyAppHttp {

    public static PLATFORMS = {
        Android: { name: "ANDROID", osType: 1 },
        iOS: { name: "IOS", osType: 2 }
    };

    public static PAGES = {
        WELCOME: {
            component: 'WelcomePage',
            showMenu: false,
            addToStack: false,
            baseStackPage: true
        },
        OTP: {
            component: 'OtpPage',
            showMenu: false,
            addToStack: false,
            baseStackPage: true
        }
    };

}
