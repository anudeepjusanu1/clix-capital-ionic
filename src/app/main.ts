import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import 'd3';
import 'nvd3';
import { AppModule } from './app.module';

platformBrowserDynamic().bootstrapModule(AppModule);
