import { Component } from '@angular/core';
import { NavController,NavParams, Platform  } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import Utils from '../../app/services/common/utils';
import MyAppHttp from '../../app/services/common/myAppHttp.service';
import { Helper } from '../../app/services/common/helper.service';
import OtpPage from '../otp/otp';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage extends Helper {
  
  public loginForm: FormGroup;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform) {
    super(navCtrl, navParams, platform);
    this.loginForm = new FormBuilder().group({
      'phoneFC': [],
    });
  }

  getOtpMessage(){
    this.goToPage('OtpPage',{ sessionIdForOTP: 12345 }, true);
  };
}
