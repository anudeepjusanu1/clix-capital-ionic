///<reference path="../../../plugins/cordova-plugin-mfp/typings/worklight.d.ts" />
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Helper } from '../../app/services/common/helper.service';
import { Chart } from 'chart.js';
declare var WLResourceRequest;

@IonicPage()
@Component({
    selector: 'page-disbursal',
    templateUrl: 'disbursal.html',
    encapsulation: ViewEncapsulation.None
})
export class DisbursalPage extends Helper {
    @ViewChild('barCanvas') barCanvas;
    barChart: any;
    data: any;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public platform: Platform) {
        super(navCtrl, navParams, platform);
        this.data = null;
        this.load();
    }

    ionViewDidLoad() {
        this.data = [2,2,2,21];

        this.barChart = new Chart(this.barCanvas.nativeElement, {
 
            type: 'bar',
            data: {
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
                datasets: [{
                    label: 'Sanctioned Disbursed',
                    data: this.data,
                    backgroundColor: [
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(54, 162, 235, 0.2)'
                    ],
                    borderColor: [
                        'rgba(54, 162, 235, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(54, 162, 235, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
 
        });
    }

    load(){
        console.log('--> called employee service');
        if (this.data) {
          // already loaded data
          return Promise.resolve(this.data);
        }
    
        // don't have the data yet
        return new Promise(resolve => {
        let dataRequest = new WLResourceRequest("/mfp/api/adapters/ClixCapitalAdapter/product",WLResourceRequest.GET);
          dataRequest.send().then((response) => {
            console.log('--> adapter response recieved', response.responseJSON.results);
            this.data = response.responseJSON.results;
            resolve(this.data);
          });
        });
      }

    dashboard(){
        this.goToPage('DashboardPage',{ sessionIdForOTP: 12345 }, true);
    }
}