import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DisbursalPage } from './disbursal';

@NgModule({
    declarations: [
        DisbursalPage
    ],
    imports: [
        IonicPageModule.forChild(DisbursalPage)
    ],
    exports: [
        DisbursalPage
    ]
})
export class DisbursalPageModule { }
