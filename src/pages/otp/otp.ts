import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { NavController,NavParams, Platform  } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import Utils from '../../app/services/common/utils';
import MyAppHttp from '../../app/services/common/myAppHttp.service';
import { Helper } from '../../app/services/common/helper.service';
@IonicPage()
@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html'
})
export class OtpPage extends Helper {
    public loginForm: FormGroup;
    public myOTP: any = {};
    public otpValidationStarted = false;
    
    constructor(public navCtrl: NavController,
      public navParams: NavParams,
      public platform: Platform) {
      super(navCtrl, navParams, platform);
      this.loginForm = new FormBuilder().group({
        'phoneFC': [],
      });
    }

    validateOTP() {
        console.log("HERE")
        this.goToPage('DashboardPage',{ sessionIdForOTP: 12345 }, true);
    };

    moveToNextFieldEvent(event, previousControl, currentControl, nextControl) {
        let keyCodeCheck = this.checkKeyCode(event, currentControl);
        if (keyCodeCheck == false) {
            this.myOTP[currentControl] = "";
            return false;
        } else {
            if (Utils.isValidInput(this.myOTP[currentControl])) {
                if (nextControl != "") {
                    document.getElementById(nextControl).focus();
                } else {
                    if (this.otpValidationStarted == false) {
                        this.validateOTP();
                    }
                }
            } else {
                if (previousControl != "") {
                    this.myOTP[previousControl] = "";
                    document.getElementById(previousControl).focus();
                }
            }
        }
    };

    focusEvent() {
        for (let lintCounter = 1; lintCounter < 7; lintCounter++) {
            let currentControl = "otp" + lintCounter;
            if (!(this.myOTP[currentControl] && this.myOTP[currentControl] != "")) {
                document.getElementById(currentControl).focus();
                return;
            }
        }
    };

    checkKeyCode(event, currentControl) {
        let keyCode = event.which || event.keyCode;
        if (keyCode == 229) { //Managing Android behavior
            if (this.myOTP[currentControl] && this.myOTP[currentControl] != "" && isNaN(this.myOTP[currentControl])) {
                event.preventDefault();
                return false;
            } else {
                return true;
            }
        } else {
            if (keyCode == 8) { // || keyCode == 13 || keyCode == 46 || keyCode == 9 || keyCode == 37 || keyCode == 39) {
                return true;
            }
            else {
                if ((keyCode >= 48 && keyCode <= 57)) { // This is for desktop Num keypad but somehow it is not working|| (keyCode >= 96 && keyCode <= 105)) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    };


  }
